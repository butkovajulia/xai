import os
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from joblib import dump, load
from sklearn.model_selection import train_test_split
import learn

random_state = 123
data = pd.read_csv('data/fifa-2018-stats.csv')
y = (data['Man of the Match'] == "Yes")
feature_names = [i for i in data.columns if data[i].dtype in [np.int64]]
X = data.loc[:, feature_names]
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state=random_state)

if os.path.exists('fifa-forest.joblib'):
    model = load('fifa-forest.joblib')
else:
    model = RandomForestClassifier(n_estimators=100, random_state=random_state).fit(train_X, train_y)
    dump(model, 'fifa-forest.joblib')

perm_i = learn.PermutationImportance(model)
perm_i.fit(val_X, val_y)
perm_i.fit(train_X, train_y)
weights, variance = perm_i.get_weights()
for feature, weight in weights.items():
    print('{feat:30}: {w} +/- {var}'.format(feat=feature, w=weight, var=variance[feature]))