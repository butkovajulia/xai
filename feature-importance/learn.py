import pandas as pd
import numpy as np
import sys
from sklearn import metrics
from random import seed
from random import randint


class PermutationImportance:

    def __init__(self, model, random_state=0):
        self.model = model
        self.random_state = random_state
        self.max_iter = 5  # number of iterations to average over when computing permutation importance
        self.perm_weights = {}
        self.perm_weights_var = {}  # max deviation from the average computed in self.perm_weights
        pass

    def fit(self, x: pd.DataFrame, y: pd.Series):
        """
        Calculates permutation importance for given x and y (average over self.max_iter runs)
        :param x: data as pandas.DataFrame
        :param y: correct labels as pandas.Series
        :return: None
        """
        x_cp = x.copy()
        # yarr = y.to_numpy()

        # y_pred = self.model.predict_proba(x_cp)
        # orig_error = metrics.log_loss(yarr, y_pred)
        orig_error = self.model.score(x_cp, y)

        self.perm_weights = {feature: 0 for feature in x_cp.columns}
        self.perm_weights_var = {feature: 0 for feature in x_cp.columns}

        # seed random number generator
        seed(self.random_state)

        perm_weights_var_max = {feature: -sys.float_info.max for feature in x_cp.columns}
        perm_weights_var_min = {feature: sys.float_info.max for feature in x_cp.columns}
        for itr in range(self.max_iter):
            # perm_weights_iter = self._fit_iteration(x_cp, yarr, orig_error)
            perm_weights_iter = self._fit_iteration(x_cp, y, orig_error)
            for feature, perm_weight in perm_weights_iter.items():
                self.perm_weights[feature] += perm_weight
                perm_weights_var_max[feature] = max(perm_weight, perm_weights_var_max[feature])
                perm_weights_var_min[feature] = min(perm_weight, perm_weights_var_min[feature])

        for feature, perm_weight in perm_weights_iter.items():
            self.perm_weights[feature] /= self.max_iter
            self.perm_weights_var[feature] = max(perm_weights_var_max[feature] - self.perm_weights[feature],
                                                 self.perm_weights[feature] - perm_weights_var_min[feature])

        self.perm_weights = {k: v for k, v in sorted(self.perm_weights.items(), key=lambda item: item[1], reverse=True)}

    def _fit_iteration(self, x: pd.DataFrame, y: pd.Series, orig_error):
        """
        Calculates 1 iteration of permutation importance for given x, y
        :param x: data as pandas.DataFrame
        :param y: correct labels as pandas.Series
        :param orig_error: error before any permutation
        :return: dictionary with features as keys and respective permutation importance as values
        """
        rand_states = {feature: randint(0, 1000) for feature in x.columns}
        perm_weights_iter = {}
        # yarr = y.to_numpy()
        for feature in x.columns:
            x_feature_orig = x[feature].copy()
            x_feature_perm = x[feature].sample(random_state=rand_states[feature], frac=1)
            x_feature_perm.index = x.index

            x.update(x_feature_perm)

            # y_perm_pred = self.model.predict_proba(x)
            # feature_error = metrics.log_loss(yarr, y_perm_pred)
            feature_error = self.model.score(x, y)
            perm_weights_iter[feature] = orig_error - feature_error
            # perm_weights_iter[feature] = feature_error - orig_error

            x.update(x_feature_orig)

        return perm_weights_iter

    def get_weights(self):
        """
        Returns 2 dictionaries.
        One is the dictionary of feature importance weights calculated during the last call to .fit(x, y).
        The weights are sorted in descending order.
        Second is the dictionary of observed (over self.max_iter runs) max deviation from the calculated average importance.
        The second dictionary is not sorted.
        :return: Dictionary {'feature label': weight}, Dictionary {'feature label': max_deviation}
        """
        return self.perm_weights, self.perm_weights_var
